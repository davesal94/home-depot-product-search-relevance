import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import org.tartarus.snowball.ext.PorterStemmer;

/*************************************************************
Author: Erwin Dave Salinas
ID: 1000785308
Class: Data Mining (CSE 4334)

**************************************************************/
public class P2 {
     
	// Stores product_description.csv information
	public static HashMap<String, ArrayList<String>> product_description = new HashMap<String, ArrayList<String>>();
	
	// Stores train.csv information
	public static HashMap<String, String> train_product_id = new HashMap<String, String>();
	public static HashMap<String, ArrayList<String>> train_product_title = new HashMap<String, ArrayList<String>>();
	public static HashMap<String, ArrayList<String>> train_product_search = new HashMap<String, ArrayList<String>>();
	public static HashMap<String, String> train_product_relevance = new HashMap<String, String>();
	public static HashMap<String, ArrayList<Double>> train_with_attributes =  new HashMap<String, ArrayList<Double>>();
	
	// Stores test.csv information
	public static HashMap<String, String> test_product_id = new HashMap<String, String>();
	public static HashMap<String, ArrayList<String>> test_product_title = new HashMap<String, ArrayList<String>>();
	public static HashMap<String, ArrayList<String>> test_product_search = new HashMap<String, ArrayList<String>>();
	public static HashMap<String, ArrayList<Double>> test_with_attributes = new HashMap<String, ArrayList<Double>>();
	public static ArrayList<String> test_keys = new ArrayList<String>();
	
	// Stores result.csv information
	public static HashMap<String, Integer> test_product_relevance = new HashMap<String, Integer>();
	
	/*******************************************************************************************
	FUNCTION: main(): 
	
	      Calls file readers to initialize HashMaps for product description, train and test. 
		  Calls cosine similarities function to find similarities
		  Calls the naives_Bayes_classifier to produce results.csv

	****************************************************************************/
	public static void main(String[] args) 
	{
		product_description_reader("product_descriptions.csv");
	    train_reader("train.csv");
	    test_reader("test.csv");
	    find_cosine_similarities_for_attributes();
	    naive_Bayes_Classifier();
	}
	
	/*******************************************************************************************
	FUNCTION: cosine_similarities_for_attributes(): 
	
	      finds cosine similarity between product title and search query
	      finds cosine similarity between product description and search query
	
	********************************************************************************************/
	public static void find_cosine_similarities_for_attributes()
	{
		    for(String key: train_product_search.keySet())
			{
				ArrayList<String> query = train_product_search.get(key);
				ArrayList<String> title = train_product_title.get(key);
				String get_productID = train_product_id.get(key);
				ArrayList<String> descrip = product_description.get(get_productID);
				Double title_query_sim = cosine_similarity(query, title);
				Double descrip_query_sim = cosine_similarity(query, descrip);
				
				
			    ArrayList<Double> attributes = new ArrayList<Double>();
				attributes.add(title_query_sim );
				attributes.add(descrip_query_sim);
				
				train_with_attributes.put(key, attributes);
				
			}
		 
		    
		    for(String key: test_product_search.keySet())
			{
				ArrayList<String> query = test_product_search.get(key);
				ArrayList<String> title = test_product_title.get(key);
				String get_productID = test_product_id.get(key);
				ArrayList<String> descrip = product_description.get(get_productID);
				Double title_query_sim = cosine_similarity(query, title);
				Double descrip_query_sim = cosine_similarity(query, descrip);
				
				
			    ArrayList<Double> attributes = new ArrayList<Double>();
				attributes.add(title_query_sim );
				attributes.add(descrip_query_sim);
				
				test_with_attributes.put(key, attributes);
				
			}
			  

	}
	
	/*******************************************************************************************
	FUNCTION: naive_Bayes_Classifier(): 
	
	      Use naives_bayes_classifier to predict relevance with (product title, search query)
	      similarity and (product description, search query) similarity as attributes.
	      
	      Attributes:
	      - (product title, search query) similarity
	      - (product description, search query) similarity
	      
	      Relevance(Classes):
	      - 1
	      - 2
	      - 3
	
	********************************************************************************************/
	public static void naive_Bayes_Classifier()
	{
		ArrayList<Double> relevance_probabilities = class_probabilities();
		double relevance_one = (relevance_probabilities.get(0)+1)/(train_product_relevance.size()+3);
    	double relevance_two = (relevance_probabilities.get(1)+1)/(train_product_relevance.size()+3);
    	double relevance_three =( relevance_probabilities.get(2)+1)/(train_product_relevance.size()+3);
    	 
    	ArrayList<Double> title_probabilities = conditional_probabilities(0);
    	double t_case1 = (title_probabilities.get(0)+1)/(relevance_probabilities.get(0)+3);
    	double t_case2 = (title_probabilities.get(1)+1)/(relevance_probabilities.get(0)+3);
    	double t_case3 = (title_probabilities.get(2)+1)/(relevance_probabilities.get(0)+3);
    	double t_case4 = (title_probabilities.get(3)+1)/(relevance_probabilities.get(1)+3);
    	double t_case5 = (title_probabilities.get(4)+1)/(relevance_probabilities.get(1)+3);
    	double t_case6 = (title_probabilities.get(5)+1)/(relevance_probabilities.get(1)+3);
    	double t_case7 = (title_probabilities.get(6)+1)/(relevance_probabilities.get(2)+3);
    	double t_case8 = (title_probabilities.get(7)+1)/(relevance_probabilities.get(2)+3);
    	double t_case9 = (title_probabilities.get(8)+1)/(relevance_probabilities.get(2)+3);
    	
    	ArrayList<Double> descrip_probabilities = conditional_probabilities(1);
    	double d_case1 = (descrip_probabilities.get(0)+1)/(relevance_probabilities.get(0)+3);
    	double d_case2 = (descrip_probabilities.get(1)+1)/(relevance_probabilities.get(0)+3);
    	double d_case3 = (descrip_probabilities.get(2)+1)/(relevance_probabilities.get(0)+3);
    	double d_case4 = (descrip_probabilities.get(3)+1)/(relevance_probabilities.get(1)+3);
    	double d_case5 = (descrip_probabilities.get(4)+1)/(relevance_probabilities.get(1)+3);
    	double d_case6 = (descrip_probabilities.get(5)+1)/(relevance_probabilities.get(1)+3);
    	double d_case7 = (descrip_probabilities.get(6)+1)/(relevance_probabilities.get(2)+3);
    	double d_case8 = (descrip_probabilities.get(7)+1)/(relevance_probabilities.get(2)+3);
    	double d_case9 = (descrip_probabilities.get(8)+1)/(relevance_probabilities.get(2)+3);
    	
    	for(String key: test_with_attributes.keySet())
    	{

    		double class1_prob = relevance_one;
    		double class2_prob = relevance_two;
    		double class3_prob = relevance_three;
    		ArrayList<Double> product = test_with_attributes.get(key);
    		if(product.get(0) <=0.18)
    		{
    			class1_prob = class1_prob*t_case1;
    			class2_prob = class2_prob*t_case4;
    			class3_prob = class3_prob*t_case7;
    		}
    		else if(product.get(0) >0.18 && product.get(0)<=0.46)
    		{
    			class1_prob = class1_prob*t_case2;
    			class2_prob = class2_prob*t_case5;
    			class3_prob = class3_prob*t_case8;
    		}
    		else if(product.get(0) >0.46)
    		{
    			class1_prob = class1_prob*t_case3;
    			class2_prob = class2_prob*t_case6;
    			class3_prob = class3_prob*t_case9;
    		}

    		if(product.get(1) <=0.18)
    		{
    			class1_prob = class1_prob*d_case1;
    			class2_prob = class2_prob*d_case4;
    			class3_prob = class3_prob*d_case7;
    		}
    		else if(product.get(1) >0.18 && product.get(1)<=0.46)
    		{
    			class1_prob = class1_prob*d_case2;
    			class2_prob = class2_prob*d_case5;
    			class3_prob = class3_prob*d_case8;
    		}
    		else if(product.get(1) >0.46)
    		{
    			class1_prob = class1_prob*d_case3;
    			class2_prob = class2_prob*d_case6;
    			class3_prob = class3_prob*d_case9;
    		}
    		

    		if(class1_prob>class2_prob && class1_prob>class3_prob)
    		{
    				test_product_relevance.put(key, 1);
    		}	
    		else if(class2_prob>class1_prob && class2_prob > class3_prob)
    		{
    				test_product_relevance.put(key, 2);
    		}
    		
    		else if(class1_prob<class3_prob && class2_prob<class3_prob)
    		{
    				test_product_relevance.put(key, 3);
    		}
 		
    	}

    	
    	String delimeter = ",";
    	String newLine = "\n";
    	
    	String column_title = "id,relevance";
    	
    	FileWriter fileWriter = null;
    	try{
    		fileWriter = new FileWriter("result.csv");
    		fileWriter.append(column_title);
    		fileWriter.append(newLine);
    		
    		for(int i=1; i<test_keys.size(); i++)
    		{
    			fileWriter.append(test_keys.get(i));
    			fileWriter.append(delimeter);
    			fileWriter.append(String.valueOf(test_product_relevance.get(test_keys.get(i))));
    			fileWriter.append(newLine);
    		}
    		
    	}
    	catch (Exception e) 
    	{
    	    e.printStackTrace();
    	} 
    	finally 
    	{
              try {
    			        fileWriter.flush();
    			        fileWriter.close();
    			   } 
              catch (IOException e) 
              {
    			      e.printStackTrace();
    		  }
         }
	}
	
	/*******************************************************************************************
	FUNCTION: conditional_probabilities
	
	   Use to facilitate of finding the conditional probability for cases for  Attributes:
	   (product title, search query) similarity (product description, search query) similarity
	   and relevance combination.
	
	********************************************************************************************/
	
	public static ArrayList<Double >conditional_probabilities(int attribute_index)
	{
		double prob1=0;
		double prob2=0;
		double prob3=0;
		double prob4=0;
		double prob5=0;
		double prob6=0;
		double prob7=0;
		double prob8=0;
		double prob9=0;
		
		ArrayList<Double> probabilities = new ArrayList<Double>();
	    for(String key: train_product_relevance.keySet())
        {
        	double cos = train_with_attributes.get(key).get(attribute_index);
        	double relevance = Double.valueOf(train_product_relevance.get(key));
        	if(relevance< 2.0 && cos<=0.18)
        	{
        		prob1++;
        	}
        	else if(relevance< 2.0 && (cos>0.18 && cos<=0.46)){
        		prob2++;
        	}
        	else if(relevance< 2.0 && (cos>0.46))
        	{
        		prob3++;
        	}
        	else if((relevance>=2.0 && relevance<=2.5 ) && cos<=0.18)
        	{
        		prob4++;
        	}
        	else if((relevance>=2.0 && relevance<=2.5 ) && (cos>0.18 && cos<=0.46)){
        		prob5++;
        	}
        	else if((relevance>=2.0 && relevance<=2.5) && (cos>0.46))
        	{
        		prob6++;
        	}
        	else if(relevance>2.5 && cos<=0.18)
        	{
        		prob7++;
        	}
        	else if(relevance>2.5 && (cos>0.18 && cos<=0.46)){
        		prob8++;
        	}
        	else if(relevance>2.5  && (cos>0.46))
        	{
        		prob9++;
        	}
        	
        } 
		
		probabilities.add(prob1);
		probabilities.add(prob2);
		probabilities.add(prob3);
		probabilities.add(prob4);
		probabilities.add(prob5);
		probabilities.add(prob6);
		probabilities.add(prob7);
		probabilities.add(prob8);
		probabilities.add(prob9);
		return probabilities;
	}
	
	/*******************************************************************************************
	FUNCTION: class_probabilities()
	
		Use to facilitate of finding the probability of getting the each classe, the relevances.
	
	********************************************************************************************/
	
	public static ArrayList<Double >class_probabilities()
	{
		double prob1=0;
		double prob2=0;
		double prob3=0;

		
		ArrayList<Double> probabilities = new ArrayList<Double>();
		for(String key: train_product_relevance.keySet())
        {
	        	double relevance = Double.valueOf(train_product_relevance.get(key));
	        	if(relevance<2.0)
	        	{
	        		prob1++;
	        	}
	        	else if(relevance>=2.0 && relevance<=2.5){
	        		prob2++;
	        	}
	        	else if(relevance>2.5)
	        	{
	        		prob3++;
	        	}
        	
        } 
		probabilities.add(prob1);
		probabilities.add(prob2);
		probabilities.add(prob3);
        
		return probabilities;
	}
	
	/*******************************************************************************************
	FUNCTION: tokenize()
	
		Use to remove stopwords and stem words.
	
	********************************************************************************************/
	
	public static ArrayList<String> tokenize(String word)
	{
		ArrayList<String> sortedstopwords = new ArrayList<String>(Arrays.asList("a", "about", "above", "after", 
				"again", "against", "all","am", "an", "and", "any", "are", "as", "at", "be", "because", "been", 
				"before", "being", "below", "between", "both", "but", "by", "can", "did", "do", "does", "doing", 
				"don", "down", "during", "each", "few", "for", "from", "further", "had", "has", "have", "having", "he",
				"her", "here", "hers", "herself", "him", "himself", "his", 
				"how", "i", "if", "in", "into", "is", "it", "its", "itself", "just", "me", "more", "most", "my", "myself", 
				"no", "nor", "not", "now", "of", "off", "on", "once", "only", "or", "other", "our", "ours", "ourselves", 
				"out", "over", "own", "s", "same", "she", "should", "so", "some", "such", "t", "than", 
				"that", "the", "their", "theirs", "them", "themselves", "then", "there", "these", "they", "this", 
				"those", "through", "to", "too", "under", "until", "up", 
				"very", "was", "we", "were", "what", "when", "where", "which", "while",
				"who", "whom", "why", "will", "with", "you", "your", "yours", "yourself", "yourselves"));
		

		String[] tokens = word.split("[^a-zA-Z']+");
		ArrayList <String> finals = new ArrayList<String>();

		for(int i=0; i<tokens.length; i++)
		{
			if(sortedstopwords.contains(tokens[i].toLowerCase()))
			{
				continue;
			}
			else{
				PorterStemmer s = new PorterStemmer();
				s.setCurrent(tokens[i].toLowerCase());
				s.stem();
				String stemmedWord= s.getCurrent();
				finals.add(stemmedWord);
			}
		} 
		return finals;
		
	}
	
	/*******************************************************************************************
	FUNCTION: product_description_reader(String csvFile)
	
		Use to read product_description.csv
	
	********************************************************************************************/
	
	public static void product_description_reader(String csvFile)
	{
		try 
		{
			BufferedReader br = null;
			String line = "";
			br = new BufferedReader(new FileReader(csvFile));
			
			while ((line = br.readLine()) != null) {
				String[] product = line.split(",");
				if(product[0].equals("product_uid"))
				{
					continue;
				}
				product_description.put(product[0], tokenize(product[1]));
			}
			br.close();
			
			
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/*******************************************************************************************
	FUNCTION: test_reader(String csvFile)
	
		Use to read test.csv
	
	********************************************************************************************/
	public static void test_reader(String csvFile)
	{
		try
		{
			BufferedReader br = null;
			String line = "";
			br = new BufferedReader(new FileReader(csvFile));
			
			while ((line = br.readLine()) != null) {
				String[] trains = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				if(trains[0].equals("id"))
				{
					continue;
				}
				test_product_id.put(trains[0],trains[1]);
				test_product_title.put(trains[0],tokenize(trains[2]));
				test_product_search.put(trains[0], tokenize(trains[3]));
				test_keys.add(trains[0]);
			}
			br.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/*******************************************************************************************
	FUNCTION: cosine_similarity (ArrayList<String> list1, ArrayList<String> list2)
	
		Use to find cosine similarity
	
	********************************************************************************************/
	public static double cosine_similarity (ArrayList<String> list1, ArrayList<String> list2)
	{
		HashMap<String, Double> each_product_terms = new HashMap<String, Double>();
        HashMap<String, Double> each_product_description = new HashMap<String, Double>();
		ArrayList<String> allWordsNoDup = new ArrayList<String>();
		
		double each_item_vector = 0;
		double description_vector = 0;
		for(int i=0; i<list1.size(); i++)
		{
			
			if(!each_product_terms.containsKey(list1.get(i)))
			{
				int count = Collections.frequency(list1, list1.get(i));
				each_product_terms.put(list1.get(i), (1+Math.log10(count)));
				
				each_item_vector = each_item_vector + 
						Math.pow(each_product_terms.get(list1.get(i)),2.0);
			}
			if(!allWordsNoDup.contains(list1.get(i)))
			{
				allWordsNoDup.add(list1.get(i));
			}
        }
		if(list2!=null)
		{
			for(int i=0; i<list2.size(); i++)
			{
				
				if(!each_product_description.containsKey(list2.get(i)))
				{
					int count = Collections.frequency(list2, list2.get(i));
					each_product_description.put(list2.get(i), (1+Math.log10(count)));
					description_vector = description_vector + 
							Math.pow(each_product_description.get(list2.get(i)),2.0);
				}
				if(!allWordsNoDup.contains(list2.get(i)))
				{
					allWordsNoDup.add(list2.get(i));
				}
	        }
		}
		
		double cosSimilarity=0;
		for(int j=0; j<allWordsNoDup.size(); j++)
		{
			if(each_product_terms.containsKey(allWordsNoDup.get(j)) && 
					each_product_description.containsKey(allWordsNoDup.get(j)))
			{
				each_product_terms.put(allWordsNoDup.get(j), each_product_terms.get(allWordsNoDup.get(j))/
						Math.sqrt(each_item_vector));
				each_product_description.put(allWordsNoDup.get(j), each_product_description.get(allWordsNoDup.get(j))/
						Math.sqrt(description_vector));
				cosSimilarity = cosSimilarity + (each_product_terms.get(allWordsNoDup.get(j))*
						each_product_description.get(allWordsNoDup.get(j)));
				
			}
			
		} 
	
		return cosSimilarity;
	}
	
	/*******************************************************************************************
	FUNCTION: train_reader(String csvFile)
	
		Use to read train.csv
	
	********************************************************************************************/
	public static void train_reader(String csvFile)
	{
		try
		{
			BufferedReader br = null;
			String line = "";
			br = new BufferedReader(new FileReader(csvFile));
			
			while ((line = br.readLine()) != null) {
				String[] trains = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				if(trains[0].equals("id"))
				{
					continue;
				}
				train_product_id.put(trains[0],trains[1]);
				train_product_title.put(trains[0],tokenize(trains[2]));
				train_product_search.put(trains[0], tokenize(trains[3]));
				train_product_relevance.put(trains[0], trains[4]);
			}
			br.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	
	

}

